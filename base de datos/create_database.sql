SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- Creación de la base de datos
CREATE DATABASE IF NOT EXISTS bd_proyecto_cine DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE bd_proyecto_cine;

-- Creación de las tablas
CREATE TABLE sexos (
	sex_id int(11) NOT NULL AUTO_INCREMENT,
	sex_valor varchar(10) UNIQUE NOT NULL,
	PRIMARY KEY (sex_id)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE generos (
	gen_id int(11) NOT NULL AUTO_INCREMENT,
	gen_nombre varchar(100) UNIQUE NOT NULL,
	PRIMARY KEY (gen_id)  
) ENGINE=InnoDB AUTO_INCREMENT=17;

CREATE TABLE paises (
	pai_id int(11) NOT NULL AUTO_INCREMENT,
	pai_codigo char(2) UNIQUE NOT NULL,
	pai_nombre varchar(100) UNIQUE NOT NULL,
	PRIMARY KEY (pai_id)
) ENGINE=InnoDB AUTO_INCREMENT=21;

CREATE TABLE usuarios (
	usu_id int(11) NOT NULL AUTO_INCREMENT,
	usu_email varchar(100) UNIQUE NOT NULL,
	usu_pass varchar(100) NOT NULL,
	PRIMARY KEY (usu_id)
) ENGINE=InnoDB AUTO_INCREMENT=2;

CREATE TABLE actores (
	act_id int(11) NOT NULL AUTO_INCREMENT,
	act_nacionalidad int(11) NOT NULL,
	act_sexo int(11) NOT NULL,
	act_nombre varchar(200) NOT NULL,
	act_fecha_nacimiento date NOT NULL,
	PRIMARY KEY (act_id),
	CONSTRAINT fk_act_pai FOREIGN KEY (act_nacionalidad) REFERENCES paises (pai_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_act_sex FOREIGN KEY (act_sexo) REFERENCES sexos (sex_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE directores (
	dir_id int(11) NOT NULL AUTO_INCREMENT,
	dir_nacionalidad int(11) NOT NULL,
	dir_sexo int(11) NOT NULL,
	dir_nombre varchar(200) NOT NULL,
	dir_fecha_nacimiento date NOT NULL,
	PRIMARY KEY (dir_id),
	CONSTRAINT fk_dir_pai FOREIGN KEY (dir_nacionalidad) REFERENCES paises (pai_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_dir_sex FOREIGN KEY (dir_sexo) REFERENCES sexos (sex_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE peliculas (
	pel_id int(11) NOT NULL AUTO_INCREMENT,
	pel_director int(11) NOT NULL,
	pel_pais int(11) NOT NULL,
	pel_titulo varchar(100) NOT NULL,
	pel_anyo smallint(4) unsigned NOT NULL,
	pel_imagen varchar(250) NOT NULL,
	pel_sinopsis text NOT NULL,
	pel_fecha_subida timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (pel_id),
	CONSTRAINT fk_pel_dir FOREIGN KEY (pel_director) REFERENCES directores (dir_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_pel_pai FOREIGN KEY (pel_pais) REFERENCES paises (pai_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE trailers (
	tra_id int(11) NOT NULL AUTO_INCREMENT,
	tra_pelicula int(11) NOT NULL,
	tra_video varchar(250) NOT NULL,
	PRIMARY KEY (tra_id),
	CONSTRAINT fk_tra_pel FOREIGN KEY (tra_pelicula) REFERENCES peliculas (pel_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE visitas (
	vis_id int(11) NOT NULL AUTO_INCREMENT,
	vis_usuario int(11) NOT NULL,
	vis_pelicula int(11) NOT NULL,
	vis_fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (vis_id),
	CONSTRAINT fk_vis_pel FOREIGN KEY (vis_pelicula) REFERENCES peliculas (pel_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_vis_usu FOREIGN KEY (vis_usuario) REFERENCES usuarios (usu_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE repartos (
	rep_actor int(11) NOT NULL,
	rep_pelicula int(11) NOT NULL,
	PRIMARY KEY (rep_actor,rep_pelicula),
	CONSTRAINT fk_rep_act FOREIGN KEY (rep_actor) REFERENCES actores (act_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_rep_pel FOREIGN KEY (rep_pelicula) REFERENCES peliculas (pel_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE generos_peliculas (
	gpe_genero int(11) NOT NULL,
	gpe_pelicula int(11) NOT NULL,
	PRIMARY KEY (gpe_genero,gpe_pelicula),
	CONSTRAINT fk_gpe_gen FOREIGN KEY (gpe_genero) REFERENCES generos (gen_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_gpe_pel FOREIGN KEY (gpe_pelicula) REFERENCES peliculas (pel_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- Insección de datos
INSERT INTO generos (gen_id, gen_nombre) VALUES(9, 'Acción');
INSERT INTO generos (gen_id, gen_nombre) VALUES(10, 'Animación');
INSERT INTO generos (gen_id, gen_nombre) VALUES(11, 'Aventura');
INSERT INTO generos (gen_id, gen_nombre) VALUES(12, 'Bélico');
INSERT INTO generos (gen_id, gen_nombre) VALUES(1, 'Ciencia ficción ');
INSERT INTO generos (gen_id, gen_nombre) VALUES(4, 'Comedia');
INSERT INTO generos (gen_id, gen_nombre) VALUES(6, 'Documental');
INSERT INTO generos (gen_id, gen_nombre) VALUES(5, 'Drama');
INSERT INTO generos (gen_id, gen_nombre) VALUES(8, 'Histórico');
INSERT INTO generos (gen_id, gen_nombre) VALUES(15, 'Musical');
INSERT INTO generos (gen_id, gen_nombre) VALUES(14, 'Romance');
INSERT INTO generos (gen_id, gen_nombre) VALUES(13, 'Serie B');
INSERT INTO generos (gen_id, gen_nombre) VALUES(7, 'Serie TV');
INSERT INTO generos (gen_id, gen_nombre) VALUES(2, 'Terror');
INSERT INTO generos (gen_id, gen_nombre) VALUES(3, 'Thriller');
INSERT INTO generos (gen_id, gen_nombre) VALUES(16, 'Western');

INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(1, 'US', 'Estados Unidos');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(2, 'ES', 'España');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(3, 'DE', 'Alemania');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(4, 'AR', 'Argentina');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(5, 'AT', 'Austria');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(6, 'CA', 'Canadá');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(7, 'CN', 'China');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(8, 'CO', 'Colombia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(9, 'FR', 'Francia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(10, 'PT', 'Portugal');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(11, 'PL', 'Polonia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(12, 'GB', 'Reino Unido');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(13, 'RU', 'Rusia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(14, 'VE', 'Venezuela ');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(15, 'SE', 'Suecia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(16, 'PE', 'Perú');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(17, 'NL', 'Países Bajos');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(18, 'MX', 'México');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(19, 'IT', 'Italia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(20, 'GR', 'Grecia');

INSERT INTO sexos (sex_id, sex_valor) VALUES(1, 'Hombre');
INSERT INTO sexos (sex_id, sex_valor) VALUES(2, 'Mujer');

INSERT INTO usuarios (usu_email, usu_pass) VALUES('usuario@email.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1');
