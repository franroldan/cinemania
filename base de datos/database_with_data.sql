SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS bd_proyecto_cine DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE bd_proyecto_cine;

CREATE TABLE IF NOT EXISTS actores (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_nacionalidad` int(11) NOT NULL,
  `act_sexo` int(11) NOT NULL,
  `act_nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `act_fecha_nacimiento` date NOT NULL,
  PRIMARY KEY (`act_id`),
  KEY `fk_act_pai` (`act_nacionalidad`),
  KEY `fk_act_sex` (`act_sexo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO actores (act_id, act_nacionalidad, act_sexo, act_nombre, act_fecha_nacimiento) VALUES(1, 12, 2, 'Kate Winslet', '1975-10-05');
INSERT INTO actores (act_id, act_nacionalidad, act_sexo, act_nombre, act_fecha_nacimiento) VALUES(2, 1, 1, 'Leonardo DiCaprio', '1974-11-11');
INSERT INTO actores (act_id, act_nacionalidad, act_sexo, act_nombre, act_fecha_nacimiento) VALUES(3, 5, 1, 'Arnold Schwarzenegger', '1947-07-30');
INSERT INTO actores (act_id, act_nacionalidad, act_sexo, act_nombre, act_fecha_nacimiento) VALUES(4, 1, 2, 'Sigourney Weaver', '1949-10-08');

CREATE TABLE IF NOT EXISTS directores (
  `dir_id` int(11) NOT NULL AUTO_INCREMENT,
  `dir_nacionalidad` int(11) NOT NULL,
  `dir_sexo` int(11) NOT NULL,
  `dir_nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `dir_fecha_nacimiento` date NOT NULL,
  PRIMARY KEY (`dir_id`),
  KEY `fk_dir_pai` (`dir_nacionalidad`),
  KEY `fk_dir_sex` (`dir_sexo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO directores (dir_id, dir_nacionalidad, dir_sexo, dir_nombre, dir_fecha_nacimiento) VALUES(1, 1, 1, 'James Cameron', '1967-03-01');
INSERT INTO directores (dir_id, dir_nacionalidad, dir_sexo, dir_nombre, dir_fecha_nacimiento) VALUES(2, 1, 1, 'Clint Eastwood', '1944-11-23');
INSERT INTO directores (dir_id, dir_nacionalidad, dir_sexo, dir_nombre, dir_fecha_nacimiento) VALUES(3, 1, 1, 'Ridley Scott', '1937-10-30');

CREATE TABLE IF NOT EXISTS generos (
  `gen_id` int(11) NOT NULL AUTO_INCREMENT,
  `gen_nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`gen_id`),
  UNIQUE KEY `gen_nombre` (`gen_nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO generos (gen_id, gen_nombre) VALUES(9, 'Acción');
INSERT INTO generos (gen_id, gen_nombre) VALUES(10, 'Animación');
INSERT INTO generos (gen_id, gen_nombre) VALUES(11, 'Aventura');
INSERT INTO generos (gen_id, gen_nombre) VALUES(12, 'Bélico');
INSERT INTO generos (gen_id, gen_nombre) VALUES(1, 'Ciencia ficción ');
INSERT INTO generos (gen_id, gen_nombre) VALUES(4, 'Comedia');
INSERT INTO generos (gen_id, gen_nombre) VALUES(6, 'Documental');
INSERT INTO generos (gen_id, gen_nombre) VALUES(5, 'Drama');
INSERT INTO generos (gen_id, gen_nombre) VALUES(8, 'Histórico');
INSERT INTO generos (gen_id, gen_nombre) VALUES(15, 'Musical');
INSERT INTO generos (gen_id, gen_nombre) VALUES(14, 'Romance');
INSERT INTO generos (gen_id, gen_nombre) VALUES(13, 'Serie B');
INSERT INTO generos (gen_id, gen_nombre) VALUES(7, 'Serie TV');
INSERT INTO generos (gen_id, gen_nombre) VALUES(2, 'Terror');
INSERT INTO generos (gen_id, gen_nombre) VALUES(3, 'Thriller');
INSERT INTO generos (gen_id, gen_nombre) VALUES(16, 'Western');

CREATE TABLE IF NOT EXISTS generos_peliculas (
  `gpe_genero` int(11) NOT NULL,
  `gpe_pelicula` int(11) NOT NULL,
  PRIMARY KEY (`gpe_genero`,`gpe_pelicula`),
  KEY `fk_gpe_pel` (`gpe_pelicula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(1, 2);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(1, 4);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(2, 5);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(2, 7);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(5, 1);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(5, 3);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(8, 1);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(9, 4);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(9, 6);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(11, 2);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(11, 6);
INSERT INTO generos_peliculas (gpe_genero, gpe_pelicula) VALUES(14, 1);

CREATE TABLE IF NOT EXISTS paises (
  `pai_id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_codigo` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `pai_nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pai_id`),
  UNIQUE KEY `pai_codigo` (`pai_codigo`),
  UNIQUE KEY `pai_nombre` (`pai_nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(1, 'US', 'Estados Unidos');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(2, 'ES', 'España');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(3, 'DE', 'Alemania');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(4, 'AR', 'Argentina');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(5, 'AT', 'Austria');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(6, 'CA', 'Canadá');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(7, 'CN', 'China');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(8, 'CO', 'Colombia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(9, 'FR', 'Francia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(10, 'PT', 'Portugal');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(11, 'PL', 'Polonia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(12, 'GB', 'Reino Unido');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(13, 'RU', 'Rusia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(14, 'VE', 'Venezuela ');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(15, 'SE', 'Suecia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(16, 'PE', 'Perú');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(17, 'NL', 'Países Bajos');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(18, 'MX', 'México');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(19, 'IT', 'Italia');
INSERT INTO paises (pai_id, pai_codigo, pai_nombre) VALUES(20, 'GR', 'Grecia');

CREATE TABLE IF NOT EXISTS peliculas (
  `pel_id` int(11) NOT NULL AUTO_INCREMENT,
  `pel_director` int(11) NOT NULL,
  `pel_pais` int(11) NOT NULL,
  `pel_titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pel_anyo` smallint(4) UNSIGNED NOT NULL,
  `pel_imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pel_sinopsis` text COLLATE utf8_unicode_ci NOT NULL,
  `pel_fecha_subida` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pel_id`),
  KEY `fk_pel_dir` (`pel_director`),
  KEY `fk_pel_pai` (`pel_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(1, 1, 1, 'Titanic', 1997, 'titanic.jpg', 'Jack (DiCaprio), un joven artista, gana en una partida de cartas un pasaje para viajar a América en el Titanic, el trasatlántico más grande y seguro jamás construido. A bordo conoce a Rose (Kate Winslet), una joven de una buena familia venida a menos que va a contraer un matrimonio de conveniencia con Cal (Billy Zane), un millonario engreído a quien sólo interesa el prestigioso apellido de su prometida. Jack y Rose se enamoran, pero el prometido y la madre de ella ponen todo tipo de trabas a su relación. Mientras, el gigantesco y lujoso transatlántico se aproxima hacia un inmenso iceberg.', '2015-12-08 08:30:19');
INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(2, 1, 1, 'Avatar', 2009, 'avatar.jpg', 'Año 2154. Jake Sully (Sam Worthington), un ex-marine condenado a vivir en una silla de ruedas, sigue siendo, a pesar de ello, un auténtico guerrero. Precisamente por ello ha sido designado para ir a Pandora, donde algunas empresas están extrayendo un mineral extraño que podría resolver la crisis energética de la Tierra. Para contrarrestar la toxicidad de la atmósfera de Pandora, se ha creado el programa Avatar, gracias al cual los seres humanos mantienen sus conciencias unidas a un avatar: un cuerpo biológico controlado de forma remota que puede sobrevivir en el aire letal. Esos cuerpos han sido creados con ADN humano, mezclado con ADN de los nativos de Pandora, los Na''vi. Convertido en avatar, Jake puede caminar otra vez. Su misión consiste en infiltrarse entre los Na''vi, que se han convertido en el mayor obstáculo para la extracción del mineral. Pero cuando Neytiri, una bella Na''vi (Zoe Saldana), salva la vida de Jake, todo cambia: Jake, tras superar ciertas pruebas, es admitido en su clan. Mientras tanto, los hombres esperan los resultados de la misión de Jake.', '2015-12-09 05:24:25');
INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(3, 2, 1, 'Gran Torino', 2008, 'gran_torino.jpg', 'Walt Kowalski (Clint Eastwood), un veterano de la guerra de Corea (1950-1953), es un obrero jubilado del sector del automóvil que ha enviudado recientemente. Su máxima pasión es cuidar de su más preciado tesoro: un coche Gran Torino de 1972. Es un hombre inflexible y cascarrabias, al que le cuesta trabajo asimilar los cambios que se producen a su alrededor, especialmente la llegada de multitud de inmigrantes asiáticos a su barrio. Sin embargo, las circustancias harán que se vea obligado a replantearse sus ideas.', '2015-12-08 07:24:26');
INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(4, 1, 1, 'The Terminator', 1984, 'terminator.jpg', 'Los Ángeles, año 2029. Las máquinas dominan el mundo. Los rebeldes que luchan contra ellas tienen como líder a John Connor, un hombre que nació en los años ochenta. Para acabar con la rebelión, las máquinas deciden enviar al pasado a un robot -Terminator- cuya misión será eliminar a Sarah Connor, la madre de John, e impedir así su nacimiento.', '2015-12-02 03:44:41');
INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(5, 1, 1, 'Aliens: El regreso', 1986, 'aliens.jpg', 'Alien es un organismo perfecto, una máquina de matar cuya superioridad física sólo puede competir con su agresividad. La oficial Ripley y la tripulación de la nave “Nostromo” se habían enfrentado, en el pasado, a esa monstruosa criatura. Y sólo Ripley sobrevivió a la masacre. Después de vagar por el espacio durante varios años, Ripley fue rescatada. Durante ese tiempo, el planeta de Alien ha sido colonizado. Pero, de repente, se pierde la comunicación con la colonia y, para investigar los motivos, se envía una expedición de marines espaciales, capitaneados por Ripley. Allí les esperan miles de espeluznantes criaturas. Alien se ha reproducido y esta vez la lucha es por la supervivencia de la Humanidad.', '2015-12-03 10:34:29');
INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(6, 3, 1, 'Gladiator', 2001, 'gladiator.jpg', 'En el año 180, el Imperio Romano domina todo el mundo conocido. Tras una gran victoria sobre los bárbaros del norte, el anciano emperador Marco Aurelio (Richard Harris) decide transferir el poder a Máximo (Russell Crowe), bravo general de sus ejércitos y hombre de inquebrantable lealtad al imperio. Pero su hijo Cómodo (Joaquin Phoenix), que aspiraba al trono, no lo acepta y trata de asesinar a Máximo. ', '2015-12-10 15:23:15');
INSERT INTO peliculas (pel_id, pel_director, pel_pais, pel_titulo, pel_anyo, pel_imagen, pel_sinopsis, pel_fecha_subida) VALUES(7, 3, 1, 'Alien, el octavo pasajero', 1979, 'alien.jpg', 'De regreso a la Tierra, la nave de carga Nostromo interrumpe su viaje y despierta a sus siete tripulantes. El ordenador central, MADRE, ha detectado la misteriosa transmisión de una forma de vida desconocida, procedente de un planeta cercano aparentemente deshabitado. La nave se dirige entonces al extraño planeta para investigar el origen de la comunicación.', '2015-12-10 06:23:15');

CREATE TABLE IF NOT EXISTS repartos (
  `rep_actor` int(11) NOT NULL,
  `rep_pelicula` int(11) NOT NULL,
  PRIMARY KEY (`rep_actor`,`rep_pelicula`),
  KEY `fk_rep_pel` (`rep_pelicula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO repartos (rep_actor, rep_pelicula) VALUES(1, 1);
INSERT INTO repartos (rep_actor, rep_pelicula) VALUES(2, 1);
INSERT INTO repartos (rep_actor, rep_pelicula) VALUES(3, 4);
INSERT INTO repartos (rep_actor, rep_pelicula) VALUES(4, 2);
INSERT INTO repartos (rep_actor, rep_pelicula) VALUES(4, 5);
INSERT INTO repartos (rep_actor, rep_pelicula) VALUES(4, 7);

CREATE TABLE IF NOT EXISTS sexos (
  `sex_id` int(11) NOT NULL AUTO_INCREMENT,
  `sex_valor` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sex_id`),
  UNIQUE KEY `sex_valor` (`sex_valor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO sexos (sex_id, sex_valor) VALUES(1, 'Hombre');
INSERT INTO sexos (sex_id, sex_valor) VALUES(2, 'Mujer');

CREATE TABLE IF NOT EXISTS trailers (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_pelicula` int(11) NOT NULL,
  `tra_video` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`tra_id`),
  KEY `fk_tra_pel` (`tra_pelicula`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO trailers (tra_id, tra_pelicula, tra_video) VALUES(1, 6, 'gladiator_1.ogv');
INSERT INTO trailers (tra_id, tra_pelicula, tra_video) VALUES(2, 6, 'gladiator_2.ogv');

CREATE TABLE IF NOT EXISTS usuarios (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `usu_pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`usu_id`),
  UNIQUE KEY `usu_email` (`usu_email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO usuarios (usu_id, usu_email, usu_pass) VALUES(2, 'usuario@email.com', '10470c3b4b1fed12c3baac014be15fac67c6e815');

CREATE TABLE IF NOT EXISTS visitas (
  `vis_id` int(11) NOT NULL AUTO_INCREMENT,
  `vis_usuario` int(11) NOT NULL,
  `vis_pelicula` int(11) NOT NULL,
  `vis_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`vis_id`),
  KEY `fk_vis_pel` (`vis_pelicula`),
  KEY `fk_vis_usu` (`vis_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(1, 2, 6, '2015-12-11 14:40:45');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(2, 2, 6, '2015-12-11 14:41:27');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(3, 2, 6, '2015-12-11 14:42:25');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(4, 2, 7, '2015-12-11 14:42:38');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(5, 2, 2, '2015-12-11 14:42:44');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(6, 2, 2, '2015-12-11 14:42:47');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(7, 2, 7, '2015-12-11 14:51:22');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(8, 2, 7, '2015-12-11 15:01:31');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(9, 2, 7, '2015-12-11 15:02:44');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(10, 2, 7, '2015-12-11 15:03:14');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(11, 2, 1, '2015-12-11 15:03:22');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(12, 2, 1, '2015-12-11 15:04:48');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(13, 2, 1, '2015-12-11 15:04:57');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(14, 2, 1, '2015-12-11 15:05:46');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(15, 2, 1, '2015-12-11 15:06:12');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(16, 2, 1, '2015-12-11 15:07:01');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(17, 2, 2, '2015-12-11 15:07:07');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(18, 2, 7, '2015-12-11 15:08:49');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(19, 2, 5, '2015-12-15 14:03:38');
INSERT INTO visitas (vis_id, vis_usuario, vis_pelicula, vis_fecha) VALUES(20, 2, 1, '2015-12-16 06:36:17');


ALTER TABLE actores
  ADD CONSTRAINT `fk_act_pai` FOREIGN KEY (`act_nacionalidad`) REFERENCES `paises` (`pai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_act_sex` FOREIGN KEY (`act_sexo`) REFERENCES `sexos` (`sex_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE directores
  ADD CONSTRAINT `fk_dir_pai` FOREIGN KEY (`dir_nacionalidad`) REFERENCES `paises` (`pai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dir_sex` FOREIGN KEY (`dir_sexo`) REFERENCES `sexos` (`sex_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE generos_peliculas
  ADD CONSTRAINT `fk_gpe_gen` FOREIGN KEY (`gpe_genero`) REFERENCES `generos` (`gen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gpe_pel` FOREIGN KEY (`gpe_pelicula`) REFERENCES `peliculas` (`pel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE peliculas
  ADD CONSTRAINT `fk_pel_dir` FOREIGN KEY (`pel_director`) REFERENCES `directores` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pel_pai` FOREIGN KEY (`pel_pais`) REFERENCES `paises` (`pai_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE repartos
  ADD CONSTRAINT `fk_rep_act` FOREIGN KEY (`rep_actor`) REFERENCES `actores` (`act_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rep_pel` FOREIGN KEY (`rep_pelicula`) REFERENCES `peliculas` (`pel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE trailers
  ADD CONSTRAINT `fk_tra_pel` FOREIGN KEY (`tra_pelicula`) REFERENCES `peliculas` (`pel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE visitas
  ADD CONSTRAINT `fk_vis_pel` FOREIGN KEY (`vis_pelicula`) REFERENCES `peliculas` (`pel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vis_usu` FOREIGN KEY (`vis_usuario`) REFERENCES `usuarios` (`usu_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
