<div class="recommended">
	<div class="recommended-grids" style="margin-top:0;">
		<div class="recommended-info">
			<h3>Búsqueda de películas <span style="color:#21DEEF;"><?=$busqueda?></span></h3>
			<br/>
		</div>
		<?php if(!empty($peliculas)){
			foreach($peliculas as $pel){ ?>
	        <div class="col-md-3 resent-grid recommended-grid">
				<div class="resent-grid-img recommended-grid-img">
					<a href="/cinemania/peliculas/detalle/<?=$pel['pel']['pel_id']?>">
	                    <?=$this->Html->image($pel['pel']['pel_imagen'], array('alt' => 'Imagen pelicula'))?>
	                </a>
				</div>
				<div class="resent-grid-info recommended-grid-info video-info-grid">
					<h5>
	                    <a href="/cinemania/peliculas/detalle/<?=$pel['pel']['pel_id']?>" class="title">
							<?=$pel['pel']['pel_titulo']?>
						</a>
	                </h5>
					<ul>
						<li><p class="author author-info"><a class="author"><?=$pel['dir']['dir_nombre']?></a></p></li>
						<li class="right-list"><p class="views views-info"><?=$pel['pai']['pai_nombre']?> (<?=$pel['pai']['pai_codigo']?>)</p></li>
					</ul>
				</div>
			</div>
			<?php } }else{ ?>
			<div class="col-md-12 resent-grid recommended-grid">
				La búsqueda realizada no ha devuelto resultados.
			</div>
		<?php } ?>
		<div class="clearfix"> </div>
	</div>
</div>
