<div class="col-sm-12" style="margin-top:-10px;">
	<div class="song">
		<div class="song-info">
			<h1><?=$pelicula["Pelicula"]["pel_titulo"]?></h1>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="published" style="margin-top:-5px;" id="ficha">
		<h3 style="margin-top:6px;">Ficha técnica</h3>
		<hr/>
		<div class="load_more">
			<ul id="myList">
				<li>
					<p><b>Director: </b> <?=$pelicula["Director"]["dir_nombre"]?> (<?=$pelicula["Director"]["dir_edad"]?> años)</p>
				</li>
				<li>
					<p><b>Año de estreno: </b> <?=$pelicula["Pelicula"]["pel_anyo"]?></p>
				</li>
				<li>
					<p><b>País: </b> <?=$pelicula["Pais"]["pai_nombre"]?> (<?=$pelicula["Pais"]["pai_codigo"]?>)</p>
				</li>
				<li>
					<p><b>Fecha de publicación en Cinemanía: </b> <?=date("d/m/Y H:i", strtotime($pelicula["Pelicula"]["pel_fecha_subida"]))?></p>
				</li>
				<li>
					<p><b>Visitas a la ficha de la película: </b> <?=count($pelicula["Visita"])?></p>
				</li>
				<li>
					<p>
						<b>Géneros cinematográficos:</b>
						<?php foreach($pelicula["Genero"] as $gen){ ?>
							 &nbsp; <a href="/cinemania/generos/listar/<?=$gen["gen_id"]?>"><?=$gen["gen_nombre"]?></a>
						<?php } ?>
					</p>
				</li>
				<li>
					<p>
						<b>Reparto:</b>
						<?php $reparto = array();
						foreach($pelicula["Actor"] as $act){
							$reparto[] = $act["act_nombre"].' ('.$act["act_edad"].' años)';
						 }
						 echo implode(',  ',$reparto); ?>
					</p>
				</li>
				<li>
					<p><?=$pelicula["Pelicula"]["pel_sinopsis"]?></p>
				</li>
			</ul>
		</div>
	</div>

	<div class="published">
		<h3 style="margin-top:6px;">Trailers</h3>
		<hr/>
		<?php if(!empty($pelicula["Trailer"])){
			foreach($pelicula["Trailer"] as $tra){ ?>
			<video controls style="width:100%;">
				<source src="<?=$this->webroot.'files/'.$tra["tra_video"]?>" type="video/ogg" />
			</video>
			<br/><br/>
		<?php } }else{ ?>
			<p>Aun no se han subido trailer para esta película.</p>
		<?php } ?>
	</div>
</div>
<div class="clearfix"> </div>
<script>
	$(document).ready(function () {
		$("#myList li").show();
		$("iframe").css("height",$("#ficha").css("height"));
		$.ajax({
	  		method: "POST",
		  	url: "/cinemania/peliculas/visitarPelicula",
		  	data: { pelicula: "<?=$pelicula["Pelicula"]["pel_id"]?>" }
		});
	});
</script>
