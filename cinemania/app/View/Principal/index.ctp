<div class="recommended" style="margin-top:-50px;">
	<div class="recommended-grids">
		<div class="recommended-info">
			<h2>Películas más visitadas por los usuarios</h2>
			<br/>
		</div>
		<?php foreach($masVistas as $pel){ ?>
        <div class="col-md-3 resent-grid recommended-grid">
			<div class="resent-grid-img recommended-grid-img">
				<a href="/cinemania/peliculas/detalle/<?=$pel['pel']['pel_id']?>">
                    <?=$this->Html->image($pel['pel']['pel_imagen'], array('alt' => 'Imagen pelicula'))?>
                </a>
			</div>
			<div class="resent-grid-info recommended-grid-info video-info-grid">
				<h5>
                    <a href="/cinemania/peliculas/detalle/<?=$pel['pel']['pel_id']?>" class="title">
						<?=$pel['pel']['pel_titulo']?>
					</a>
                </h5>
				<ul>
					<li><p class="author author-info"><a class="author"><?=$pel['dir']['dir_nombre']?></a></p></li>
					<li class="right-list"><p class="views views-info"><?=$pel[0]['numVisitas']?> visitas</p></li>
				</ul>
			</div>
		</div>
		<?php } ?>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="recommended">
	<div class="recommended-grids">
		<div class="recommended-info">
			<h2>Películas subidas a <i>Cinemanía</i> recientemente</h2>
			<br/>
		</div>
		<?php foreach($masRecientes as $pel){ ?>
        <div class="col-md-3 resent-grid recommended-grid">
			<div class="resent-grid-img recommended-grid-img">
				<a href="/cinemania/peliculas/detalle/<?=$pel['Pelicula']['pel_id']?>">
                    <?=$this->Html->image($pel['Pelicula']['pel_imagen'], array('alt' => 'Imagen pelicula'))?>
                </a>
			</div>
			<div class="resent-grid-info recommended-grid-info video-info-grid">
				<h5>
                    <a href="/cinemania/peliculas/detalle/<?=$pel['Pelicula']['pel_id']?>" class="title">
						<?=$pel['Pelicula']['pel_titulo']?>
					</a>
                </h5>
				<ul>
					<li><p class="author author-info"><a class="author"><?=$pel['Director']['dir_nombre']?></a></p></li>
					<li class="right-list"><p class="views views-info"><?=date('d/m/Y H:i',strtotime($pel['Pelicula']['pel_fecha_subida']))?></p></li>
				</ul>
			</div>
		</div>
		<?php } ?>
		<div class="clearfix"> </div>
	</div>
</div>
