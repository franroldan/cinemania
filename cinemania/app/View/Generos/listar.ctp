<div class="recommended">
	<div class="recommended-grids" style="margin-top:0;">
		<div class="recommended-info">
			<h2>Películas del género <span style="color:#21DEEF;"><?=$genero["Genero"]["gen_nombre"]?></span></h2>
			<br/>
		</div>
		<?php foreach($peliculas as $pel){ ?>
        <div class="col-md-3 resent-grid recommended-grid">
			<div class="resent-grid-img recommended-grid-img">
				<a href="/cinemania/peliculas/detalle/<?=$pel['Pelicula']['pel_id']?>">
                    <?=$this->Html->image($pel['Pelicula']['pel_imagen'], array('alt' => 'Imagen pelicula'))?>
                </a>
			</div>
			<div class="resent-grid-info recommended-grid-info video-info-grid">
				<h5>
                    <a href="/cinemania/peliculas/detalle/<?=$pel['Pelicula']['pel_id']?>" class="title">
						<?=$pel['Pelicula']['pel_titulo']?>
					</a>
                </h5>
				<ul>
					<li><p class="author author-info"><a class="author"><?=$pel['Director']['dir_nombre']?></a></p></li>
					<li class="right-list"><p class="views views-info"><?=$pel['Pais']['pai_nombre']?> (<?=$pel['Pais']['pai_codigo']?>)</p></li>
				</ul>
			</div>
		</div>
		<?php } ?>
		<div class="clearfix"> </div>
	</div>
</div>
