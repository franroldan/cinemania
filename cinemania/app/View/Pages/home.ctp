<?php
	if(empty($this->Session->read('usuario'))){
		?><h3>Portal privado</h3>
		<p>Para acceder a la plataforma ha de loguearse como usuario</p>
		<p>
			Puede registrar o loguearse mediante la opción <b>Registro / Login</b>
			situada en la parte superior-derecha de la pantalla
		</p>
		<?php
	}else{
		?><script type="text/javascript">
			location.href = "/cinemania/principal";
		</script><?php
	}
?>
