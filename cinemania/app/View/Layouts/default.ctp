<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo $this->Html->charset(); ?>
		<title>Cinemanía - Proyecto PHP M.V.C.</title>
		<?php
			echo $this->Html->meta('icon');
			echo $this->Html->css('bootstrap.min');
			echo $this->Html->css('dashboard');
			echo $this->Html->css('popuo-box');
			echo $this->Html->css('style');
			echo $this->Html->script('jquery-1.11.1.min');
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->Html->script(array(
				'jquery-1.11.1.min',
				'responsiveslides.min',
				'modernizr.custom.min',
				'jquery.polyglot.language.switcher',
				'jquery.magnific-popup',
				'bootstrap.min'
			));
		?>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
				});
				$('.mensajesAplicacion').click(function(){
					$(this).fadeOut(2000);
				})
			});
			function buscar(){
				$("#peliculasBuscarForm").submit();
			}
			function recordar(){
				var email = $('input[name=email]').val().trim();
				if(email.length>0){
					$('input[name=recordarPass]').val(email);
					$("#accesosRecordarForm").submit();
				}
			}
		</script>
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/cinemania" style="margin-top:-7px;">
						<h1><?=$this->Html->image('logo.png', array('alt' => 'Logo Cinemania'))?></h1>
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<?php echo $this->Form->create('peliculas', array(
						'action' => 'buscar',
						'type' => 'GET',
						'class' => 'navbar-form navbar-right'
 					)); ?>
						<input type="text" name="textoBusqueda" class="form-control" placeholder="Buscar..." style="width:95%;" />
						<?=$this->Html->image('9.png', array(
							'alt' => 'Buscar',
							'style' => 'float:right;margin-top:6px;cursor:pointer;',
							'onclick' => 'javascript:buscar();'
						))?>
					<?php echo $this->Form->end(); ?>
					<div class="header-top-right">
						<div class="signin">
						<?php if(empty($this->Session->read('usuario'))){ ?>
							<a href="#small-dialog" class="play-icon popup-with-zoom-anim">Registro / Login</a>
							<!-- REGISTRO-LOGIN -->
							<div id="small-dialog" class="mfp-hide" style="max-width:40%;">
								<h2>Registro o login</h2><br/>
								<div>
									<?php echo $this->Form->create('accesos', array(
										'action' => 'acceso',
										'type' => 'POST',
										'style' => 'font-size:10pt;'
				 					)); ?>
										<p><input type="text" name="email" class="email" placeholder="Email..." required="required" pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?"/></p>
										<p><input type="password" name="pass" placeholder="Contraseña..." required="required" pattern=".{6,}" title="Tamaño mínimo: 6 caracteres" autocomplete="off" /></p>
										<p><input type="submit" value="Registrarse o acceder"/></p>
									<?php echo $this->Form->end(); ?>
									<hr/>
									<div class="forgot">
										<?php echo $this->Form->create('accesos', array(
											'action' => 'recordar',
											'type' => 'POST'
					 					)); ?>
										<input type="hidden" name="recordarPass" />
										<a href="#" onclick="javascript:recordar();">¿Olvidaste la contraseña?</a>
										<?php echo $this->Form->end(); ?>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						<?php }else{ ?>
							<a href="/cinemania/accesos/salir" id="enlaceSalir" class="play-icon popup-with-zoom-anim">
								<?=$this->Session->read('usuario')?> - Salir
							</a>
							<script type="text/javascript">
								$("#enlaceSalir").click(function(){
									window.href('/cinemania/accesos/salir');
								});
							</script>
						<?php } ?>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!-- FIN REGISTRO-LOGIN -->
		</nav>
		<div class="col-sm-3 col-md-2 sidebar">
			<div class="top-navigation">
				<div class="t-menu">MENU</div>
				<div class="t-img">
					<?=$this->Html->image('lines.png', array('alt' => 'line'))?>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="drop-navigation drop-navigation">
				<ul class="nav nav-sidebar">
					<li>
						<a href="/cinemania/principal" class="home-icon">
							<span class="glyphicon glyphicon-film" aria-hidden="true"></span> Inicio
						</a>
					</li>
					<?php foreach($generos as $g){ ?>
					<li>
						<a href="/cinemania/generos/listar/<?=$g['Genero']['gen_id']?>" class="user-icon">
							<span class="glyphicon glyphicon-film" aria-hidden="true"></span>
							<?=$g['Genero']['gen_nombre']?>
						</a>
					</li>
					<?php } ?>
				</ul>
				<div class="side-bottom" style="margin:0 auto;width:100%;">
					<div class="copyright" style="text-align:center;">
						<p>Cinemanía - Comunidad cinéfila</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="main-grids">
				<?php if(!empty($error)){ ?>
					<div class="alert alert-danger mensajesAplicacion" role="alert"><?=$error?></div>
				<?php } ?>
				<?php echo $this->Flash->render(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	</body>
</html>
