<?php
class Usuario extends AppModel {
    public $name = 'Usuario';
    public $useTable = 'usuarios';
    public $primaryKey = 'usu_id';

    protected $_schema = array(
        'usu_id' => array(
            'type' => 'integer'
        ),
        'usu_email' => array(
            'type' => 'string',
            'length' => 100
        ),
        'usu_pass' => array(
            'type' => 'string',
            'length' => 100
        )
    );
}
?>
