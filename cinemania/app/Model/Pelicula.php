<?php
class Pelicula extends AppModel {
    public $name = 'Pelicula';
    public $useTable = 'peliculas';
    public $primaryKey = 'pel_id';
    public $actsAs = array('Containable');

    protected $_schema = array(
        'pel_id' => array(
            'type' => 'int'
        ),'pel_titulo' => array(
            'type' => 'string',
            'length' => 100
        ),
        'pel_anyo' => array(
            'type' => 'integer'
        ),
        'pel_imagen' => array(
            'type' => 'string',
            'length' => 250
        ),
        'pel_sinopsis' => array(
            'type' => 'text'
        ),
        'pel_fecha_subida' => array(
            'type' => 'datetime'
        )
    );

    public $belongsTo = array(
        'Director' => array(
            'className' => 'Director',
            'foreignKey' => 'pel_director'
        ),
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'pel_pais'
        )
    );

    public $hasMany = array(
        'Trailer' => array(
            'className' => 'Trailer',
            'foreignKey' => 'tra_pelicula'
        ),'Visita' => array(
            'className' => 'Visita',
            'foreignKey' => 'vis_pelicula'
        )
    );

    public $hasAndBelongsToMany = array(
        'Actor' => array(
            'className' => 'Actor',
            'joinTable' => 'repartos',
            'foreignKey' => 'rep_pelicula',
            'associationForeignKey' => 'rep_actor'
        ),
        'Genero' => array(
            'className' => 'Genero',
            'joinTable' => 'generos_peliculas',
            'foreignKey' => 'gpe_pelicula',
            'associationForeignKey' => 'gpe_genero'
        )
    );
}
?>
