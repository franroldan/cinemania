<?php
class Visita extends AppModel {
    public $name = 'Visita';
    public $useTable = 'visitas';
    public $primaryKey = 'vis_id';

    protected $_schema = array(
        'vis_id' => array(
            'type' => 'integer'
        ),
        'vis_fecha' => array(
            'type' => 'datetime'
        )
    );

    public $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'vis_usuario'
        ),
        'Pelicula' => array(
            'className' => 'Pelicula',
            'foreignKey' => 'vis_pelicula'
        )
    );
}
?>
