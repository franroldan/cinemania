<?php
class Actor extends AppModel {
    public $name = 'Actor';
    public $useTable = 'actores';
    public $primaryKey = 'act_id';

    protected $_schema = array(
        'act_id' => array(
            'type' => 'integer'
        ),
        'act_nombre' => array(
            'type' => 'string',
            'length' => 200
        ),
        'act_fecha_nacimiento' => array(
            'type' => 'date'
        )
    );

    public $belongsTo = array(
        'Sexo' => array(
            'className' => 'Sexo',
            'foreignKey' => 'act_sexo'
        ),
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'act_nacionalidad'
        )
    );

    public $virtualFields = array(
        'act_edad' => "FLOOR(DATEDIFF(CURDATE(),act_fecha_nacimiento)/365)"
    );
}
?>
