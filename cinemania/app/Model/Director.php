<?php
class Director extends AppModel {
    public $name = 'Director';
    public $useTable = 'directores';
    public $primaryKey = 'dir_id';
    public $actsAs = array('Containable');

    protected $_schema = array(
        'dir_nombre' => array(
            'type' => 'string',
            'length' => 200
        ),
        'dir_fecha_nacimiento' => array(
            'type' => 'date'
        )
    );

    public $belongsTo = array(
        'Sexo' => array(
            'className' => 'Sexo',
            'foreignKey' => 'dir_sexo'
        ),
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'dir_nacionalidad'
        )
    );

    public $virtualFields = array(
        'dir_edad' => "FLOOR(DATEDIFF(CURDATE(),dir_fecha_nacimiento)/365)"
    );
}
?>
