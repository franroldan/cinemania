<?php
class Genero extends AppModel {
    public $name = 'Genero';
    public $useTable = 'generos';
    public $primaryKey = 'gen_id';

    protected $_schema = array(
        'gen_id' => array(
            'type' => 'integer'
        ),
        'gen_nombre' => array(
            'type' => 'string',
            'length' => 100
        )
    );

    public $hasAndBelongsToMany = array(
        'Pelicula' => array(
            'className' => 'Pelicula',
            'joinTable' => 'generos_peliculas',
            'foreignKey' => 'gpe_genero',
            'associationForeignKey' => 'gpe_pelicula'
        )
    );
}
?>
