<?php
class Trailer extends AppModel {
    public $name = 'Trailer';
    public $useTable = 'trailers';
    public $primaryKey = 'tra_id';

    protected $_schema = array(
        'tra_id' => array(
            'type' => 'integer'
        ),
        'tra_video' => array(
            'type' => 'string',
            'length' => 250
        )
    );

    public $belongsTo = array(
        'Pelicula' => array(
            'className' => 'Pelicula',
            'foreignKey' => 'tra_pelicula'
        )
    );
}
?>
