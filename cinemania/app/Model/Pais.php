<?php
class Pais extends AppModel {
    public $name = 'Pais';
    public $useTable = 'paises';
    public $primaryKey = 'pai_id';
    public $actsAs = array('Containable');

    protected $_schema = array(
        'pai_id' => array(
            'type' => 'integer'
        ),
        'pai_nombre' => array(
            'type' => 'string',
            'length' => 100
        ),
        'pai_codigo' => array(
            'type' => 'string',
            'length' => 2
        )
    );
}
?>
