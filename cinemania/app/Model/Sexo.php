<?php
class Sexo extends AppModel {
    public $name = 'Sexo';
    public $useTable = 'sexos';
    public $primaryKey = 'sex_id';

    protected $_schema = array(
        'sex_id' => array(
            'type' => 'integer'
        ),
        'sex_valor' => array(
            'type' => 'string',
            'length' => 10
        )
    );
}
?>
