<?php
class PeliculasController extends AppController {

    var $name = "Peliculas";
    var $uses = array('Pelicula','Visita');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    // Obtener el detalle de una película concreta
    function detalle($_id = null){
        // Comprobar si existe la película
        $pelicula = $this->Pelicula->find("first",array(
            "recursive" => 1,
            "conditions" => array("Pelicula.pel_id" => $_id)
        ));
        // Control de seguridad
        if(empty($pelicula) || empty($this->Session->read('usuario'))){
            $this->redirect("/");
        }
        // Envío de variables a la vista
        $this->set(array(
            "pelicula" => $pelicula
        ));
    }

    // Función que añade una visita a una película
    function visitarPelicula(){
        // No renderizar vista ya que la función se llama por Ajax
        $this->autoRender = false;
        $this->layout = 'ajax';
        if(!empty($this->request->data["pelicula"])){
            // Comprobar si existe la película
            $pelicula = $this->Pelicula->find("first",array(
                "recursive" => 1,
                "conditions" => array("Pelicula.pel_id" => (int)$this->request->data["pelicula"])
            ));
            if(!empty($pelicula) && !empty($this->Session->read('usuario'))){
                // Guardar la visita en BD
                $this->Visita->create();
                $this->Visita->save(array(
                    "vis_usuario" => (int)$this->Session->read('id_usuario'),
                    "vis_pelicula" => (int)$this->request->data["pelicula"]
                ));
                echo 1;
                exit;
            }
        }
        echo 0;
    }

    // Buscar películas en:
    // - Título
    // - Psinopsis
    // - Nombre del director
    // - Nombre de los actores del reparto
    function buscar(){
        $textoBusqueda = (!empty($this->request->query["textoBusqueda"])) ? trim($this->request->query["textoBusqueda"]) : '';
        // Control de seguridad
        if(empty($this->Session->read('usuario'))){
            $this->redirect("/");
        }
        if(empty($textoBusqueda)){
            $this->Session->write('error','Error: debe especificar un texto de búsuqeda');
            $this->redirect($this->referer());
        }
        // Cargar películas de la base de datos
        $peliculas = $this->Pelicula->query("
            SELECT pel.*, dir.*, pai.*
            FROM peliculas pel
            JOIN directores dir ON (pel.pel_director = dir.dir_id)
            JOIN paises pai ON (pai.pai_id = pel.pel_pais)
            WHERE pel.pel_titulo LIKE '%{$textoBusqueda}%'
            OR pel.pel_sinopsis LIKE '%{$textoBusqueda}%'
            OR dir.dir_nombre LIKE '%{$textoBusqueda}%'
            OR pel.pel_id IN (
                SELECT DISTINCT(pel_id)
                FROM peliculas
                JOIN repartos ON (rep_pelicula = pel_id)
                JOIN actores ON (rep_actor = act_id)
                WHERE act_nombre LIKE '%{$textoBusqueda}%'
            )
        ");
        // Envío de variables a la vista
        $this->set(array(
            "busqueda" => $textoBusqueda,
            "peliculas" => $peliculas
        ));
    }
}

?>
