<?php
class PrincipalController extends AppController {

    var $name = "Principal";
    var $uses = array('Pelicula','Director','Pais');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    // Carga las películas recomendadas:
    // - 4 más vistos
    // - 4 más recientes
    public function index(){
        // Control de seguridad
        if(empty($this->Session->read('usuario'))){
            $this->redirect("/");
        }
        // 4 películas más recientes
        $masRecientes = $this->Pelicula->find("all",array(
            "recursive" => 1,
            "contain" => array("Director","Pais"),
            "order" => "pel_fecha_subida DESC",
            "limit" => 4
        ));
        // 4 películas más vistas
        $masVistas = $this->Pelicula->query("
            SELECT COUNT(vis.vis_id) as numVisitas, pel.*, dir.*
            FROM peliculas pel
            JOIN directores dir ON (pel.pel_director = dir.dir_id)
            LEFT JOIN visitas vis ON (pel.pel_id = vis.vis_pelicula)
            GROUP BY pel.pel_id
            ORDER BY numVisitas DESC, pel.pel_fecha_subida DESC
            LIMIT 4
        ");
        // Envío de variables a la vista
        $this->set(array(
            "masRecientes" => $masRecientes,
            "masVistas" => $masVistas
        ));
    }
}
?>
