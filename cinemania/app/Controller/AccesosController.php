<?php
class AccesosController extends AppController {

    var $name = "Accesos";
    var $uses = array('Usuario');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    // Función que loguea o registra a un usuario
    function acceso(){
        if(!empty($this->data) && empty($this->Session->read('usuario'))){
            $email = trim($this->data['email']);
            $pass = trim($this->data['pass']);
            if(empty($email) || empty($pass)){
                $this->Session->write('error','Error: debe especificar un email y una contraseña');
            }else{
                $pass = sha1(md5($pass));
                $usuario = $this->Usuario->find("first",array(
                    "recursive" => -1,
                    "conditions" => array(
                        "Usuario.usu_email" => $email
                    )
                ));
                if(empty($usuario)){
                    // Se crea el usuario
                    $this->Usuario->create(array(
                        "usu_email" => $email,
                        "usu_pass" => $pass
                    ));
                    $this->Usuario->save();
                    $this->Session->write('error','Info: Usuario registrado correctamente');
                    $usuario = $this->Usuario->find("first",array(
                        "recursive" => -1,
                        "conditions" => array(
                            "Usuario.usu_email" => $email
                        )
                    ));
                    $this->Session->write('error','Info: gracias por registrarse en Cinemanía');
                }else if($usuario["Usuario"]["usu_pass"] != $pass){
                    // Error de acceso
                    $this->Session->write('error','Error: clave de acceso incorrecta');
                    $this->redirect("/");
                    exit;
                }
                // Se registra en session el usuario
                $this->Session->write('usuario',$email);
                $this->Session->write('id_usuario',$usuario["Usuario"]["usu_id"]);
            }
        }else{
            $this->Session->write('error','Error: no se ha podido completar el proceso de registro/login');
        }
        $this->redirect("/");
    }

    // Función que recurdar a un usuario su contraseña vía email
    function recordar(){
        $email = (!empty($this->request->data["recordarPass"])) ? trim($this->request->data["recordarPass"]) : '';
        // Control de seguridad
        if(empty($email) || !empty($this->Session->read('usuario'))){
            $this->redirect("/");
        }
        // Comprobar si existe el usuario
        $usuario = $this->Usuario->find("first",array(
            "recursive" => -1,
            "conditions" => array(
                "Usuario.usu_email" => $email
            )
        ));
        if(!empty($usuario)){
            App::uses('CakeEmail', 'Network/Email');
            $Email = new CakeEmail();
            $Email->from(array('info@cinemania.com' => 'Cinemania'))
                ->to($usuario["Usuario"]["usu_email"])
                ->subject('Recordar contraseña')
                ->send('Su clave de acceso es: '.$usuario["Usuario"]["usu_pass"]);
        }else{
            $this->Session->write('error','Error: el email indicado no está registrado');
        }
        $this->redirect("/");
    }

    function salir(){
        $this->Session->delete('usuario');
        $this->Session->delete('id_usuario');
        $this->redirect("/");
    }
}

?>
