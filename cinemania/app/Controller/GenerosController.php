<?php
class GenerosController extends AppController {

    var $name = "Generos";
    var $uses = array('Genero','Pelicula');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    // Lista las películas de un género concreto
    function listar($_id = null){
        // Comprobar si existe el género
        $genero = $this->Genero->find("first",array(
            "recursive" => -1,
            "conditions" => array("Genero.gen_id" => $_id)
        ));
        // Control de seguridad
        if(empty($genero) || empty($this->Session->read('usuario'))){
            $this->redirect("/");
        }
        // Cargar películas del género
        $peliculas = array();
        $query = $this->Genero->find("all",array(
            "conditions" => array("Genero.gen_id" => $_id),
            "contain" => array("Pelicula")
        ));
        if(!empty($query[0]["Pelicula"])){
            foreach($query[0]["Pelicula"] as $pel){
                $peliculas[] = $this->Pelicula->find("first",array(
                    "conditions" => array("Pelicula.pel_id" => $pel["pel_id"])
                ));
            }
        }
        // Envío de variables a la vista
        $this->set(array(
            "peliculas" => $peliculas,
            "genero" => $genero
        ));
    }
}

?>
